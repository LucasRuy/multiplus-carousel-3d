import gulp from 'gulp';
import requireDir from 'require-dir';

requireDir('./gulp_files');

gulp.task('default', ['browserSync', 'pug', 'stylus', 'watch', 'copy', 'bundle']);
