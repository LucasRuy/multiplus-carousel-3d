import gulp from 'gulp';

import imagemin from 'gulp-imagemin';
import svg from 'gulp-svg-sprite';

const javascript = './app/src/plugins/*.js';
const images = './app/static/images/**/*';
const imagesSVG = './app/static/images/icons/*.svg';

gulp.task('copy:jsplugins', () => {
	gulp.src(javascript)
	.pipe(gulp.dest('dist/assets/javascript'))
});

gulp.task('copy:images', () => {
	gulp.src(images)
	.pipe(imagemin())
	.pipe(gulp.dest('dist/assets/images'))
});

gulp.task('copy:svg', () => {
	return gulp.src(imagesSVG)
	.pipe(svg({
		mode: {
			symbol: {
				render: {
					css: false,
					scss: false
				},
				dest: 'icons',
				sprite: 'icons.svg',
			}
		}
	}))
	.pipe(gulp.dest('./dist/assets/images'));
});

gulp.task('copy', ['copy:jsplugins', 'copy:images', 'copy:svg']);