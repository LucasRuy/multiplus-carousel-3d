import gulp from 'gulp';

import gutil from 'gulp-util';
import livereload from 'gulp-livereload';
import rename from 'gulp-rename';
import uglify from 'gulp-uglify';
import babelify from 'babelify';
import browserify from 'browserify';
import buffer from 'vinyl-buffer';
import source from 'vinyl-source-stream';

const config = {
	js: {
		src: './app/src/script.js',
		outputDir: './dist/assets/javascript',
		outputFile: 'bundle.js'
	}
};

const bundle = (bundler) => {
	bundler
		.bundle()
		.pipe(source(config.js.src))
		.pipe(buffer())
		.pipe(rename(config.js.outputFile))
		.pipe(gulp.dest(config.js.outputDir))
		.pipe(livereload());
};

gulp.task('bundle', () => {
	const bundler = browserify(config.js.src)
	.transform(babelify, { presets: ['es2015'] });

	bundle(bundler);
});