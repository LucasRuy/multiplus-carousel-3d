import gulp from 'gulp';
import stylus from 'gulp-stylus';

gulp.task('stylus', () => {
	return gulp.src('app/static/stylesheet/application.styl')
		.pipe(stylus({ compress: false }))
		.pipe(gulp.dest('dist/assets/stylesheet'));

});
