import gulp from 'gulp';

import uglify from 'gulp-uglify';
import imagemin from 'gulp-imagemin';
import pug from 'gulp-pug';
import stylus from 'gulp-stylus';
import svg from 'gulp-svg-sprite';

const images = './app/static/images/**/*';
const imagesSVG = './app/static/images/icons/*.svg';

gulp.task('build:images', () => {
	gulp.src('./app/static/images/*')
	.pipe(imagemin())
	.pipe(gulp.dest('./build/assets/images'))
});

gulp.task('build:svg', () => {
	return gulp.src(imagesSVG)
	.pipe(svg({
		mode: {
			symbol: {
				render: {
					css: false,
					scss: false
				},
				dest: 'icons',
				sprite: 'icons.svg',
			}
		}
	}))
	.pipe(gulp.dest('./build/assets/images'));
});

gulp.task('build:js', () => {
	return gulp.src('./dist/assets/javascript/bundle.js')
	.pipe(uglify())
	.pipe(gulp.dest('./build/assets/javascript'))
});

gulp.task('build:stylus', () => {
	return gulp.src('app/static/stylesheet/application.styl')
		.pipe(stylus({ compress: true }))
		.pipe(gulp.dest('./build/assets/stylesheet'));
});

gulp.task('build:pug', () => {
	return gulp.src('app/static/templates/index.pug')
		.pipe(pug({
			pretty: false
		}))
		.pipe(gulp.dest('./build'));
});

gulp.task('build', ['build:stylus', 'build:pug', 'build:svg', 'build:images', 'build:js']);