### Used Tecnologies

* pug template engine - build your HTML files
* stylus - build your CSS files
* gulp - automate your taks
* browser-sync - auto reload your browser when save changed files

### How to use

* Clone the repository and install all dependencies with commands.
- run `npm install`
- run `bower install`

* Start your gulpfile with the commands
- run `gulp` to execute default task
- run `gulp pug` to compile .PUG files to .HTML files
- run `gulp stylus` to compile .STYL files to .CSS files
- run `gulp copy` to copy all images files to `/dist` folder
- run `gulp build` to compile all files

* Or
- run `npm start` to init `gulpfile.babel.js`
- run `npm run build`	to compile your project

* After run command to execute your default task, you have two link of acess your landing page.
- Local: <http://localhost:3000/>
- Exteranl: <http://your-ip:3000/>


### License
#### The MIT License
* Copyright (c) 2017 Lucas Ruy <lukas.r@hotmail.com>

- Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the 'Software'), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

- The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

- THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.