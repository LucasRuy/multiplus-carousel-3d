// Shortcut of document.querySelector();
// Use example: 
// -- One element: const example = get.element('.element');
// -- Multiple elements: const example = get.elements('.elements');
export const get = {
	element: function(element) {
		return document.querySelector(element);
	},
	elements: function(elements) {
		return document.querySelectorAll(elements);
	}
};

// Remove classes method;
// Use example: 
// -- One element: removeClass(Element, 'RemovedClassName')
// -- Multiple elements: removeClass(Elements, 'RemovedClassName')
export const removeClass = (element, elementClass) => {
	if(element.length > 0){
		for (let e = 0; e < element; e += 1) {
			element[e].classList.remove(elementClass);
		}
	}	else {
		element.classList.remove(elementClass);
	}
};