const InitSlick = () => {
	$(".x-carousel__wrapper").Cloud9Carousel( {
	  buttonLeft: $(".x-carousel__controls__prev"),
	  buttonRight: $(".x-carousel__controls__next"),
	  itemClass: 'x-carousel__item',
	  autoPlay: 0,
	  bringToFront: true,
	  speed: 2
	} );

	const itemsCarousel = document.querySelectorAll('.x-carousel__item__content');
  const buttonNext = document.querySelector('.x-carousel__controls__next');
  const buttonPrev = document.querySelector('.x-carousel__controls__prev');
  
  let index = 0;
  
  const setActive = (el) => {
    for(let i = 0; i < itemsCarousel.length; i++) {
      itemsCarousel[i].classList.remove('is-active');
    }
    el[index].classList.add('is-active');
    console.log(index);
  };
  
  const next = () => {
    if (index <= itemsCarousel.length -1) {
      index++;
      setActive(itemsCarousel);
      
      if (index === itemsCarousel.length -1) {
        index = -1;
      }      
    }
  };
  
  const prev = () => {
    if (index <= itemsCarousel.length) {
      if (index === 0) {
        index = itemsCarousel.length;
      }
      
      index--;
      setActive(itemsCarousel);
    }
  };
  
  buttonNext.addEventListener('click', next, false);
  buttonPrev.addEventListener('click', prev, false);

};

export default InitSlick;